<?php

namespace Acme\Bundle\XmlConnectorBundle\Writer;

use Acme\Bundle\XmlConnectorBundle\Controller\SendEmailStep as Mailer;
use Akeneo\Tool\Component\Batch\Item\FlushableInterface;
use Akeneo\Tool\Component\Batch\Item\InitializableInterface;
use Akeneo\Tool\Component\Batch\Item\ItemWriterInterface;
use Akeneo\Tool\Component\Batch\Step\StepExecutionAwareInterface;
use Akeneo\Tool\Component\Connector\Writer\File\AbstractFileWriter;
use Akeneo\Tool\Component\Connector\Writer\File\ArchivableWriterInterface;
use Swift_Mailer;
use Swift_SmtpTransport;

class XmlWriter extends AbstractFileWriter implements
    ItemWriterInterface,
    InitializableInterface,
    FlushableInterface,
    ArchivableWriterInterface,
    StepExecutionAwareInterface
{



    /** @var array */
    protected $writtenFiles = [];

    /** @var \XMLWriter **/
    protected $xml;

    /**
     * {@inheritdoc}
     */
    public function initialize()

    {

        $transport = (new Swift_SmtpTransport('smtp.gmail.com', 25, 'tls'))
            ->setUsername('anagbojunior@gmail.com')
            ->setPassword('');
        $mail = new Swift_Mailer($transport);
        $mailer = new Mailer($mail);
        $transport->getPassword() == '' ? $transport->setPassword('null') : $mailer->indexAction($mail);


        if (null === $this->xml) {
            $filePath = $this->stepExecution->getJobParameters()->get('filePath');

            $this->xml = new \XMLWriter();
            $this->xml->openURI($filePath);
            $this->xml->startDocument('1.0', 'UTF-8');
            $this->xml->setIndent(4);
            $this->xml->startElement('products');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getWrittenFiles()
    {
        return $this->writtenFiles;
    }

    /**
     * {@inheritdoc}
     */
    public function write(array $items)
    {

        $exportDirectory = dirname($this->getPath());
        if (!is_dir($exportDirectory)) {
            $this->localFs->mkdir($exportDirectory);
        }

        foreach ($items as $item) {
            $this->xml->startElement('product');
            foreach ($item as $property => $value) {
                $this->xml->writeAttribute($property, $value);
            }
            $this->xml->endElement();



        }
    }

    /**
     * {@inheritdoc}
     */
    public function flush()
    {
        $this->xml->endElement();
        $this->xml->endDocument();
        $this->xml->flush();

        $this->writtenFiles = [$this->stepExecution->getJobParameters()->get('filePath')];
    }
}
